//
//  UIAlertView+Message.h
//  RigParking
//
//  Created by Mason Lyngby on 2/23/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Message)

+ (void) quickTitle:(NSString *)title message:(NSString *)message;
+ (void) error:(NSString *)error;

@end
