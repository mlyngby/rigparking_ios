//
//  UIView+FindAndResignFirstResponder.m
//  RigParking
//
//  Created by Mason Lyngby on 2/20/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "UIView+FindAndResignFirstResponder.h"

@implementation UIView (FindAndResignFirstResponder)

- (BOOL)findAndResignFirstResponder
{
    if (self.isFirstResponder) {
        [self resignFirstResponder];
        return YES;
    }
    for (UIView *subView in self.subviews) {
        if ([subView findAndResignFirstResponder])
            return YES;
    }
    return NO;
}

@end
