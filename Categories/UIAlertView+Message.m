//
//  UIAlertView+Message.m
//  RigParking
//
//  Created by Mason Lyngby on 2/23/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "UIAlertView+Message.h"

@implementation UIAlertView (Message)

+ (void) quickTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [alertView show];
}

+ (void) error:(NSString *)error {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}

@end
