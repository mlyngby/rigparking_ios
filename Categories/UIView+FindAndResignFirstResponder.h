//
//  UIView+FindAndResignFirstResponder.h
//  RigParking
//
//  Created by Mason Lyngby on 2/20/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FindAndResignFirstResponder)

- (BOOL)findAndResignFirstResponder;

@end
