//
//  Common.h
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#ifndef RigParking_Common_h
#define RigParking_Common_h


#pragma mark - Constants
#define kBaseColor 0x0174a1
//#define kRestAPIBaseURL @"http://69.164.194.167"
#define kRestAPIBaseURL @"http://127.0.0.1:9393"
#define kDefaultRadiusKilometers 100

#pragma mark - Notifications
#define kUserLocationPinpointedNotification @"UserLocationPinpointedNotification"
#define kUserLocationNotAuthorizedNotification @"UserLocationNotAuthorizedNotification"
#define kUserLoggedInNotification @"UserLoggedInNotification"
#define kUserLoggedOutNotification @"UserLoggedOutNotification"

#pragma mark - Dictionary Keys
#define kUserLocationDictionaryKey @"UserLocationDictionaryKey"

#pragma mark - Macros
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define APP ((RPApplication *) [UIApplication sharedApplication])
#define APPDEL ((RPAppDelegate *) APP.delegate)

#endif

#import "RPApplication.h"
#import "RPViewController.h"
#import "UIView+FindAndResignFirstResponder.h"
#import "UIAlertView+Message.h"
#import "AFNetworking.h"