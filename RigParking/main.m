//
//  main.m
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, @"RPApplication", NSStringFromClass([RPAppDelegate class]));
    }
}
