//
//  RPParkingLocation.h
//  RigParking
//
//  Created by Mason Lyngby on 2/8/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define kRPParkingLatitudeKey @"lat"
#define kRPParkingLongitudeKey @"lon"
#define kRPParkingCityKey @"city"
#define kRPParkingLocationName @"locationName"
#define kRPParkingSpaceKey @"parkingSpaces"

@interface RPParkingLocation : NSObject <MKAnnotation>

+ (id) parkingLocationFromDictionary:(NSDictionary *)dictionary;

- (id) initWithLocation:(CLLocationCoordinate2D)coordinate;

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *locationName;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) NSInteger parkingSpaces;

@end
