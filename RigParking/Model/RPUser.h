//
//  RPUser.h
//  RigParking
//
//  Created by Mason Lyngby on 2/20/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPUser : NSObject

- (id) initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName userName:(NSString *)userName password:(NSString *)password;

@property (nonatomic, strong) NSString * userName;
@property (nonatomic, strong) NSString * password;
@property (nonatomic, strong) NSString * email;

- (NSDictionary *) asDictionary;

@end
