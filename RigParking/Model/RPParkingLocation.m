//
//  RPParkingLocation.m
//  RigParking
//
//  Created by Mason Lyngby on 2/8/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPParkingLocation.h"

@implementation RPParkingLocation

+ (id) parkingLocationFromDictionary:(NSDictionary *)dictionary {
    NSString *latitude = [dictionary objectForKey:kRPParkingLatitudeKey];
    NSString *longitude = [dictionary objectForKey:kRPParkingLongitudeKey];
    
    CLLocationCoordinate2D locCoord = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    
    RPParkingLocation *parkingLoc = [[RPParkingLocation alloc] initWithLocation:locCoord];

    parkingLoc.city = [dictionary objectForKey:kRPParkingCityKey];
    parkingLoc.locationName = [dictionary objectForKey:kRPParkingLocationName];
    
    NSNumber *parkingSpacesNum = [dictionary objectForKey:kRPParkingSpaceKey];
    parkingLoc.parkingSpaces = [parkingSpacesNum integerValue];
    
    return parkingLoc;
}

- (id) initWithLocation:(CLLocationCoordinate2D)coordinate {
    self = [super init];
    if (self) {
        _coordinate = coordinate;
    }
    return self;
}

#pragma mark - MKAnnotation
- (NSString *) title {
    return _locationName;
}

- (NSString *) subtitle {
    return _city;
}

@end
