//
//  RPUser.m
//  RigParking
//
//  Created by Mason Lyngby on 2/20/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPUser.h"

@implementation RPUser

- (id) initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName userName:(NSString *)userName password:(NSString *)password {
    
    self = [super init];
    if (self) {

    }
    
    return self;
}

- (NSDictionary *) asDictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:5];
    [dictionary setObject:_userName forKey:@"userName"];
    [dictionary setObject:_password forKey:@"password"];
    [dictionary setObject:_email forKey:@"email"];
    
    return dictionary;
}

@end
