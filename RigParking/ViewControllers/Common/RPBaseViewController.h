//
//  RPBaseViewController.h
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPBaseViewController : UIViewController

@property (nonatomic, assign) UISearchBar * searchBar;
@property (nonatomic, retain) NSMutableArray * parkingLocations;

@end
