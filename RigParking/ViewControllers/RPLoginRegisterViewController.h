//
//  RPRegisterViewController.h
//  RigParking
//
//  Created by Mason Lyngby on 2/19/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@class RPuser;

@protocol RPLoginRegisterDelegate <NSObject>

- (void) didSuccessfullyLogin:(RPUser *)user;
- (void) didFailToLogin;

@end

@interface RPLoginRegisterViewController : RPBaseViewController<UITextFieldDelegate>

- (IBAction) onCancelTapped:(id)sender;
- (IBAction) onRegisterTapped:(id)sender;
- (IBAction) onLoginTapped:(id)sender;
- (IBAction) toggleRegisterLogin:(id)sender;

@property (nonatomic, strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic, strong) IBOutlet UITextField *userName;
@property (nonatomic, strong) IBOutlet UITextField *email;
@property (nonatomic, strong) IBOutlet UITextField *password;
@property (nonatomic, strong) IBOutlet UITextField *confirm;

@property (nonatomic, strong) IBOutlet UITextField *loginEmail;
@property (nonatomic, strong) IBOutlet UITextField *loginPassword;

@property (nonatomic, strong) IBOutlet UIView * registerView;
@property (nonatomic, strong) IBOutlet UIView * loginView;

@property (nonatomic, weak) id<RPLoginRegisterDelegate> delegate;

@end
