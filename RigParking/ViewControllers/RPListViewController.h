//
//  RPListViewController.h
//  RigParking
//
//  Created by Mason Lyngby on 2/2/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPBaseViewController.h"

@interface RPListViewController : RPBaseViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
