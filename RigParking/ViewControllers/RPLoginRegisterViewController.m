//
//  RPRegisterViewController.m
//  RigParking
//
//  Created by Mason Lyngby on 2/19/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPLoginRegisterViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RPUser.h"
#import "UIAlertView+Message.h"

@interface RPLoginRegisterViewController ()

@end

@implementation RPLoginRegisterViewController {
    BOOL _loginShowing;
    UITextField * _activeField;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self registerForKeyboardNotifications];
    }
    return self;
}

- (void) dealloc {
    [self clearKeyboardNotifications];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view addSubview:_loginView];
    [self.view addSubview:_registerView];

    DLog(@"%@", NSStringFromCGSize(_scrollView.contentSize));
    
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_scrollView.bounds), CGRectGetHeight(_scrollView.bounds) + 150);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    DLog(@"touchesBegan");
    [self.view findAndResignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)clearKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Actions
- (IBAction)onCancelTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onRegisterTapped:(id)sender {
    BOOL valid = YES;
    
    if (!_userName.text || _userName.text.length < 8) {
        _userName.layer.borderWidth = 1.0;
        _userName.layer.borderColor = [UIColor redColor].CGColor;
        
        _userName.placeholder = @"Username (8 chars minimum)";
        
        valid = NO;
    }
    else {
        _userName.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    if (!_email.text || _email.text.length < 8) {
        _email.layer.borderWidth = 1.0;
        _email.layer.borderColor = [UIColor redColor].CGColor;
        
        _email.placeholder = @"Email (Invalid email format)";
        
        valid = NO;
    }
    else {
        _email.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    if(!_password.text || _password.text.length < 8) {
        _password.layer.borderWidth = 1.0;
        _password.layer.borderColor = [UIColor redColor].CGColor;

        _password.placeholder = @"Password (At least 8 letters/numbers)";
        valid = NO;
    }
    else if([_password.text compare:_confirm.text] != NSOrderedSame) {
        _password.layer.borderWidth = 1.0;
        _password.layer.borderColor = [UIColor redColor].CGColor;
        _confirm.layer.borderWidth = 1.0;
        _confirm.layer.borderColor = [UIColor redColor].CGColor;

        _password.placeholder = @"Password (Must match)";
        _confirm.placeholder = @"Confirm (Must match)";
        valid = NO;
    }
    else {
        _password.layer.borderColor = [UIColor clearColor].CGColor;
    }

    if(!_confirm.text || _confirm.text.length < 8) {
        _confirm.layer.borderWidth = 1.0;
        _confirm.layer.borderColor = [UIColor redColor].CGColor;
                valid = NO;
        
    }
    else {
        _confirm.layer.borderColor = [UIColor clearColor].CGColor;

    }
    
    if (!valid) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Registration Error" message:@"Invalid Entries" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else {
        RPUser * user = [[RPUser alloc] init];
        user.userName   = _userName.text;
        user.password   = _password.text;
        user.email      = _email.text;
        
        NSDictionary *params = [user asDictionary];
        DLog(@"%@", params);
        
        NSURLRequest *request = [APP.restClient requestWithMethod:@"PUT" path:@"/user/register" parameters:params];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            DLog(@"Success, %@", JSON);
            
            if (JSON) {
                NSDictionary *userDict = (NSDictionary *)JSON;
                RPUser *newUser = [[RPUser alloc] init];
                newUser.email = [userDict valueForKey:@"email"];
                newUser.password = [userDict valueForKey:@"password"];
                newUser.userName = [userDict valueForKey:@"userName"];
                
                APP.loggedInUser = newUser;
                APP.loggedIn = YES;
                
                [UIAlertView quickTitle:@"Success" message:@"Welcome to Rig Parking"];
                
                [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            }
            else {
                [UIAlertView error:@"Something went wrong registering.  Please try again later"];
            }
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            DLog(@"Failure: %@", [error localizedDescription]);
        }];
        
        [operation start];
    }
}

- (IBAction)onLoginTapped:(id)sender {
    NSString *email = _loginEmail.text;
    NSString *password = _loginPassword.text;
    
    BOOL valid = YES;
    
    if (!_loginEmail.text || _loginEmail.text.length < 8) {
        _loginEmail.layer.borderColor = [UIColor redColor].CGColor;
        _loginEmail.layer.borderWidth = 1.0;
        _loginEmail.placeholder = @"Email (Please enter a valid email.)";
        valid = NO;
    }
    else {
        _loginEmail.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    
    if (!_loginPassword.text || _loginPassword.text.length < 8) {
        _loginPassword.layer.borderWidth = 1.0;
        _loginPassword.layer.borderColor = [UIColor redColor].CGColor;
        _loginPassword.placeholder = @"Password (At least 8 letters/numbers).";
        valid = NO;
    }
    else {
        _loginPassword.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    if (!valid) {
        return;
    }
    
    NSDictionary *params = @{@"email" : email, @"password" : password};
    
    NSURLRequest *request = [APP.restClient requestWithMethod:@"PUT" path:@"/user/login" parameters:params];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        DLog(@"Success, %@", JSON);
        [UIAlertView quickTitle:@"Success" message:@"You are now logged in."];
        APP.loggedIn = YES;
        
        NSDictionary *userInfo = (NSDictionary *)JSON;
        RPUser *loggedInUser = [userInfo objectForKey:@"user"];
        
        [self.delegate didSuccessfullyLogin:loggedInUser];

        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        DLog(@"Failure: %@", [error localizedDescription]);
        
        [UIAlertView error:@"Login Failure."];
    }];
    
    [operation start];
    
}

- (IBAction)toggleRegisterLogin:(id)sender {
    UIView *hideView = nil;
    UIView *showView = nil;
    UIViewAnimationOptions direction;
    
    if (!_loginShowing) {
        hideView = _registerView;
        showView = _loginView;
        direction = UIViewAnimationOptionTransitionFlipFromLeft;
    }
    else {
        hideView = _loginView;
        showView = _registerView;
        direction = UIViewAnimationOptionTransitionFlipFromRight;
    }
    
    [UIView transitionFromView:hideView toView:showView duration:0.5f options:UIViewAnimationOptionShowHideTransitionViews|direction completion:^(BOOL finished) {
        [self.view sendSubviewToBack:hideView];
    }];

    _loginShowing = !_loginShowing;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _activeField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    _activeField = nil;
}


#pragma mark - Scroll the content to the textfield if partially covered
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, _activeField.frame.origin.y-kbSize.height);
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}


@end
