//
//  RPCheckinViewController.h
//  RigParking
//
//  Created by Mason Lyngby on 2/19/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPBaseViewController.h"
#import "RPParkingLocation.h"

@interface RPCheckinViewController : RPBaseViewController

@property (nonatomic, strong) RPParkingLocation * parkingLocation;

@end
