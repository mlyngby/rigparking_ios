    //
//  RPMapViewController.m
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

// Show the default map while attempting to get the user's location
// Once user location is available, zoom the map to the region
// Once the region is rendered, plot the points on the map
// Cache the plotted points unless a map pan/zoom operation is performed

#import "RPMapViewController.h"
#import "RPParkingLocation.h"
#import "RPMapCalloutView.h"
#import "UIAlertView+Message.h"
#import "RPCheckinViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kMapImageGray   @"annotation_gray.png"
#define kMapImageGreen  @"annotation_green.png"
#define kMapImageOrange @"annotation_orange.png"
#define kMapImageRed    @"annotation_red.png"

NSString *kSmallParkingSpace = @"parking_type_small.png";

@interface RPMapViewController ()
@property (nonatomic, strong) UIButton * redoSearchButton;
@end

@implementation RPMapViewController {
    RPBackendServer * _backendServer;
    
    BOOL            _mapReady;
    BOOL            _showingRedoSearch;
    BOOL            _mapZoomedToRegion;
    BOOL            _showingCallout;
    
    SMCalloutView   * _calloutView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocationDidUpdate:) name:kUserLocationPinpointedNotification object:nil];
        
        _backendServer = [[RPBackendServer alloc] init];
        _backendServer.delegate = self;
        
        _calloutView = [SMCalloutView new];
        _calloutView.title = @"Callout Title";
        _calloutView.subtitle = @"Callout Subtitle";
        _calloutView.delegate = self;
        _calloutView.tag = -999;
        
        UIButton *calloutButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCheckin)];
        [calloutButton addGestureRecognizer:tapRecognizer];
        _calloutView.rightAccessoryView = calloutButton;
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserLocationPinpointedNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [APP setNetworkActivityIndicatorVisible:YES];
    if ([APP.locationManager beginLocatingUser]) {
        DLog(@"Can get it, look for a notification soon.");
        
    }
    else {
        DLog(@"Nope, can't get it");
        [APP setNetworkActivityIndicatorVisible:NO];
    }
    self.mapView.showsUserLocation = YES;
    MKUserTrackingBarButtonItem *locButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];

    self.toolbar.items = @[locButton];
    
    _redoSearchButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_redoSearchButton addTarget:self action:@selector(redoSearch:) forControlEvents:UIControlEventTouchUpInside];
    [_redoSearchButton setTitle:@"redo search?" forState:UIControlStateNormal];
    _redoSearchButton.hidden = YES;

    _redoSearchButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    
    [self.mapView addSubview:_redoSearchButton];

}

- (void) viewDidUnload {
    [super viewDidUnload];
    
    self.redoSearchButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //TODO: don't dismiss if it came from the callout view.
    
    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
    UIView *touchedView = [self.view hitTest:locationPoint withEvent:event];
    
    DLog(@"touched %@", touchedView);
    
    if (![touchedView isKindOfClass:[RPCustomAnnotationView class]]) {
        [_calloutView dismissCalloutAnimated:YES];
    }
    else {
        // don't dismiss
        DLog(@"Don't dismiss");
    }
}

#pragma mark - Private Methods
- (void) queryBackendWithMapBoundaries {
    MKCoordinateRegion region = _mapView.region;
    CLLocationCoordinate2D center = region.center;
    CLLocationCoordinate2D northWestCorner, southEastCorner;
    northWestCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0);
    northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0);
    southEastCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0);
    southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0);
    
    [_backendServer searchLocationsWithNwCorner:northWestCorner seCorner:southEastCorner];
}

- (CGFloat)calcDistance {
    
    CGPoint westMidPoint = CGPointMake(CGRectGetMinX(_mapView.frame), CGRectGetMidY(_mapView.frame));
    CLLocationCoordinate2D nwCoord = [_mapView convertPoint:westMidPoint toCoordinateFromView:self.view];
    CLLocation *nwLocation = [[CLLocation alloc] initWithLatitude:nwCoord.latitude longitude:nwCoord.longitude];
    
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:_userCoordinate.latitude longitude:_userCoordinate.longitude];
    
    CLLocationDistance distanceKm = ceil([userLocation distanceFromLocation:nwLocation] / 1000);
    
    DLog(@"%f", distanceKm);

    
    return distanceKm;
}

- (void) adjustMapToFitAndQueryBackend:(CLLocationCoordinate2D)coordinate {    
    MKCoordinateRegion userRegion = MKCoordinateRegionMakeWithDistance(coordinate, 100000, 100000);
    MKCoordinateRegion fitRegion = [_mapView regionThatFits:userRegion];
    
    [_mapView setRegion:fitRegion animated:YES];
}

- (void) removePins {
    [_mapView removeAnnotations:_mapView.annotations];
}

- (void) addPins:(NSArray *)annotations {
    
    for (RPParkingLocation *parking in annotations) {
        DLog(@"%f %f", parking.coordinate.latitude, parking.coordinate.longitude);
    }
    
    [_mapView addAnnotations:annotations];
    
}

- (BOOL) coordinateDifferent:(CLLocationCoordinate2D)fromCoord toCoord:(CLLocationCoordinate2D)toCoord {
    return round(fromCoord.latitude) != round(toCoord.latitude) || round(fromCoord.longitude) != round(toCoord.longitude);
}

- (void) promptRedoSearch {
    if (_mapReady && !_showingRedoSearch && [self coordinateDifferent:_mapView.centerCoordinate toCoord:_userCoordinate]) {
        DLog(@"promptRedoSearch");
        CGSize size = CGSizeMake(120, 35);

        CGPoint point = CGPointMake(self.view.center.x - (size.width / 2), CGRectGetMaxY(_mapView.frame)-40);
        CGRect searchFrame = {point, size};
        _redoSearchButton.frame = searchFrame;
        
        _redoSearchButton.hidden = NO;
            _mapReady = YES;
    }
}



- (UIImage *) mapImageBasedOnParkingSpaces:(NSInteger)parkingSpaces {
    if (parkingSpaces > 0 && parkingSpaces < 10) {
        return [UIImage imageNamed:kMapImageRed];
    }
    else if(parkingSpaces >= 10 && parkingSpaces < 50) {
        return [UIImage imageNamed:kMapImageOrange];
    }
    else if(parkingSpaces >= 50) {
        return [UIImage imageNamed:kMapImageGreen];
    }
    
    return [UIImage imageNamed:kMapImageGray];
}

- (void) popoverForMapAnnotation:(MKAnnotationView *)annotationView {
    
    RPMapCalloutView *customView = [[RPMapCalloutView alloc] initWithFrame:CGRectMake(0, 0, 150, 90)];
    customView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.0];
    customView.parkingLocation = (RPParkingLocation *)annotationView.annotation;
    
    _calloutView.contentView = customView;
    
    [_calloutView presentCalloutFromRect:annotationView.bounds inView:annotationView constrainedToView:self.mapView permittedArrowDirections:SMCalloutArrowDirectionAny animated:YES];
}

#pragma mark - Actions
-(void)userLocationDidUpdate:(NSNotification *)notification {
    CLLocation *userLocation = notification.userInfo[kUserLocationDictionaryKey];
    _userCoordinate = userLocation.coordinate;
    DLog(@"userLocation Notification lat %+.6f, lon %+.6f", userLocation.coordinate.latitude, userLocation.coordinate.longitude);
    
    [APP setNetworkActivityIndicatorVisible:NO];
    
    [self adjustMapToFitAndQueryBackend:userLocation.coordinate];
    _mapReady = YES;
}

-(void)redoSearch:(id)sender {
    _redoSearchButton.hidden = YES;
    [self queryBackendWithMapBoundaries];
}

-(void)showCheckin {
    if (!APP.loggedIn) {
//        [UIAlertView quickTitle:@"Login" message:@"Please Login"];
        RPLoginRegisterViewController *loginRegisterVC = [[RPLoginRegisterViewController alloc] initWithNibName:nil bundle:nil];
        loginRegisterVC.delegate = self;
        [self presentViewController:loginRegisterVC animated:YES completion:nil];
    }
    else {
        DLog(@"%@", self.navigationController);
        RPCheckinViewController *checkinViewController = [[RPCheckinViewController alloc] initWithNibName:nil bundle:nil];
        
    }
    
}

#pragma mark - MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    RPParkingLocation *parkingLoc = (RPParkingLocation *)annotation;

    static NSString * annotationId = @"customAnnotationId";
//    RPCustomAnnotationView *annotationView = [[RPCustomAnnotationView alloc] init];
    RPCustomAnnotationView *annotationView = (RPCustomAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:annotationId];
    
    if (!annotationView) {
        annotationView = [[RPCustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationId];
        
        annotationView.image = [self mapImageBasedOnParkingSpaces:parkingLoc.parkingSpaces];
        CGPoint centerOffset = annotationView.center;
        centerOffset.y = -annotationView.frame.size.height / 2;
        annotationView.centerOffset = centerOffset;
        
        annotationView.canShowCallout = NO;
        annotationView.calloutView = _calloutView;

    }
    else {
        annotationView.annotation = annotation;
    }
    
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (_mapReady && !_mapZoomedToRegion) {
        [self queryBackendWithMapBoundaries];
    }
    else {
        [self promptRedoSearch];
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
//    if (_calloutView.window) {
//        [_calloutView dismissCalloutAnimated:NO];
//    }
    
    [self popoverForMapAnnotation:view];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
//    [_calloutView dismissCalloutAnimated:YES];
}

#pragma mark - RPBackendServerDelegate
- (void) backendServer:(RPBackendServer *)server didFetchNearbyLocationsForCoordinate:(CLLocationCoordinate2D)coordinate withDistanceInKilimoters:(CGFloat)kilometers parkingLocations:(NSArray *)parkingLocations {
    
    [self removePins];
    [self addPins:parkingLocations];
    _mapZoomedToRegion = YES;
}

- (void) backendServer:(RPBackendServer *)server didFetchLocationsWithinCoordinates:(CLLocationCoordinate2D)nwCorner seCorner:(CLLocationCoordinate2D)seCorner  parkingLocations:(NSArray *)parkingLocations {
    
    _mapZoomedToRegion = YES;
    
    [self removePins];
    [self performSelector:@selector(addPins:) withObject:parkingLocations afterDelay:.5];
}

#pragma mark - RPLoginUserDelegate
- (void) didSuccessfullyLogin:(RPUser *)user {
    
}

- (void) didFailToLogin {
    
}

#pragma mark - SMCalloutViewDelegate 
// Called after the callout view appears on screen, or after the appearance animation is complete.
- (void)calloutViewDidAppear:(SMCalloutView *)calloutView {
    DLog(@"calloutViewDidAppear");
}

// Called after the callout view is removed from the screen, or after the disappearance animation is complete.
- (void)calloutViewDidDisappear:(SMCalloutView *)calloutView {
    DLog(@"calloutViewDidDisappear");
}

@end

@implementation RPCustomAnnotationView

- (id) initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect frame = CGRectMake(0, 0, 30, 38);
        self.frame = frame;
        
        self.opaque = NO;
        
        self.image = [UIImage imageNamed:@"annotation_gray.png"];
    }
    
    return self;
}

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *calloutMaybe = [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
    NSLog(@"hitTest %@", calloutMaybe);
    return calloutMaybe ?: [super hitTest:point withEvent:event];
}

@end

@interface MKMapView (UIGestureRecognizer)

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;

@end

@implementation RPMapView

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    DLog(@"%@", [gestureRecognizer.view class]);
    if ([gestureRecognizer.view isKindOfClass:[UIControl class]]) {
        return NO;
    }
    else {
        return [super gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
    }
}

@end
