//
//  RPMapViewController.h
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPBaseViewController.h"
#import "SMCalloutView.h"
#import "RPLoginRegisterViewController.h"
#import <MapKit/MapKit.h>

extern NSString *kSmallParkingSpace;

@class RPMapView;

@interface RPMapViewController : RPBaseViewController <MKMapViewDelegate, RPBackendServerDelegate, SMCalloutViewDelegate, RPLoginRegisterDelegate>
@property (strong, nonatomic) IBOutlet RPMapView *mapView;
@property (assign, nonatomic) CLLocationCoordinate2D userCoordinate;
@property (strong, nonatomic) IBOutlet UIToolbar * toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *userBarButtonItem;

@end

@interface RPCustomAnnotationView : MKAnnotationView

@property (strong, nonatomic) SMCalloutView * calloutView;

@end

@interface RPMapView : MKMapView

@end