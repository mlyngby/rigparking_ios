//
//  RPMapCalloutView.h
//  RigParking
//
//  Created by Mason Lyngby on 3/23/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RPParkingLocation.h"

typedef enum ParkingSpaceFullness {
    ParkingSpaceFullnessEmpty,
    ParkingSpaceFullnessQuarter,
    ParkingSpaceFullnessHalf,
    ParkingSpaceFullness3Quarter,
    ParkingSpaceFullnessFull
} ParkingSpaceFullness;

typedef enum ParkingSpaceSizeLevel {
    ParkingSpaceSizeLevelSmall,
    ParkingSpaceSizeLevelMedium,
    ParkingSpaceSizeLevelLarge
} ParkingSpaceSizeLevel;

@interface RPMapCalloutView : UIView

@property (nonatomic, strong) UIImage * parkingSpaceImage;
@property (nonatomic, strong) RPParkingLocation * parkingLocation;

@end
