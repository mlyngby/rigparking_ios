//
//  RPMapCalloutView.m
//  RigParking
//
//  Created by Mason Lyngby on 3/23/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPMapCalloutView.h"
#import <QuartzCore/QuartzCore.h>

@implementation RPMapCalloutView {
    UIImageView * _parkingSpaceImageView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _parkingSpaceImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        
//        _parkingSpaceImageView.layer.cornerRadius = 10.0;
//        _parkingSpaceImageView.clipsToBounds = YES;
        
//        [self addSubview:_parkingSpaceImageView];
        
    }
    return self;
}

//- (void) layoutSubviews {
//    _parkingSpaceImageView.frame = CGRectMake(10, 10, 83, 100);
//}

//- (void) setParkingSpaceImage:(UIImage *)parkingSpaceImage {
//    _parkingSpaceImageView.image = parkingSpaceImage;
//    [self setNeedsLayout];
//}

- (UIColor *) colorForParkingSpace {
    
    if (_parkingLocation.parkingSpaces < 10) {
        return [UIColor redColor];
    }
    else if(_parkingLocation.parkingSpaces >= 10 && _parkingLocation.parkingSpaces < 50) {
        return [UIColor colorWithRed:(251.0/255.0) green:(213.0/255.0) blue:(50.0/255.0) alpha:1.0];
    }
    else if(_parkingLocation.parkingSpaces >= 50) {
//        return [UIColor greenColor];
        return [UIColor colorWithRed:153.0/255.0 green:215.0/255.0 blue:104.0/255.0 alpha:1.0];
    }
    else {
        return [UIColor redColor];
    }
}

- (ParkingSpaceSizeLevel) sizeLevel {
    if (_parkingLocation.parkingSpaces < 10) {
        return ParkingSpaceSizeLevelSmall;
    }
    else if(_parkingLocation.parkingSpaces >= 10 && _parkingLocation.parkingSpaces < 50) {
        return ParkingSpaceSizeLevelMedium;
    }
    else if(_parkingLocation.parkingSpaces >= 50) {
        return ParkingSpaceSizeLevelLarge;
    }

    return ParkingSpaceSizeLevelSmall;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Drawing code
    UIColor *bottomRectColor = [self colorForParkingSpace];
    UIColor *filledBarColor = [self colorForParkingSpace];
    UIColor *unfilledBarColor = [UIColor darkGrayColor];
    UIColor *boxColor = [UIColor blackColor];
    UIColor *textSizeColor = [UIColor whiteColor];
//    UIColor *parkingSpacesColor = [UIColor blackColor];
    
    ParkingSpaceSizeLevel sizeLevel = [self sizeLevel];
    
    NSInteger baseY = 15;
    
    
    CGContextMoveToPoint(context, 20, baseY + 51);
    CGContextAddLineToPoint(context, 45, baseY + 51);
    
    CGContextSetLineWidth(context, 12.0);
    CGContextSetLineCap(context, kCGLineCapRound);

    CGContextSetStrokeColorWithColor(context, bottomRectColor.CGColor);    
    CGContextStrokePath(context);
    
    CGContextSetLineWidth(context, 7);
    CGContextMoveToPoint(context, 22, baseY + 30);
    CGContextAddLineToPoint(context, 22, baseY + 40);
    CGContextSetStrokeColorWithColor(context, filledBarColor.CGColor);
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, 32, baseY + 20);
    CGContextAddLineToPoint(context, 32, baseY + 40);
    CGContextSetStrokeColorWithColor(context, (sizeLevel > ParkingSpaceSizeLevelSmall) ? filledBarColor.CGColor : unfilledBarColor.CGColor);
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, 42, baseY + 10);
    CGContextAddLineToPoint(context, 42, baseY + 40);
    CGContextSetStrokeColorWithColor(context, (sizeLevel > ParkingSpaceSizeLevelMedium) ? filledBarColor.CGColor : unfilledBarColor.CGColor);
    CGContextStrokePath(context);
    
    CGRect borderRect = CGRectMake(10, baseY, 45, 60);
    CGContextAddRect(context, borderRect);
    CGContextSetLineCap(context, kCGLineCapButt);
    CGContextSetLineWidth(context, 2);
    CGContextSetStrokeColorWithColor(context, boxColor.CGColor);
    CGContextStrokePath(context);
    
    
//    CGRect bottomRect = CGRectOffset(borderRect, 0, CGRectGetHeight(borderRect)+3);
//    bottomRect.size.height = 16;
//    CGContextAddRect(context, bottomRect);
//    CGContextStrokePath(context);
   
    CGContextSetFillColorWithColor(context, textSizeColor.CGColor);
    NSString *text;
    switch (sizeLevel) {
        case ParkingSpaceSizeLevelSmall:
            text = @"< 10";
            break;
        case ParkingSpaceSizeLevelMedium:
            text = @"< 50";
            break;
        case ParkingSpaceSizeLevelLarge:
            text = @"> 50";
            break;
        default:
            break;
    }
//    NSString *text = [NSString stringWithFormat:@"< %d", sizeLevel];
    UIFont *font = [UIFont fontWithName:@"AmericanTypewriter" size:11];
//    [text drawAtPoint:CGPointMake(21, 85) withFont:font];
    CGPoint sizePt = CGPointMake(21, baseY+44.5);
    [text drawAtPoint:sizePt withFont:font   ];
//
//    CGContextSetFillColorWithColor(context, parkingSpacesColor.CGColor);
//    NSString *parkingSpaces = @"4";
//    UIFont *parkingFont = [UIFont fontWithName:@"AmericanTypewriter-Bold" size:12];
//    [parkingSpaces drawInRect:bottomRect withFont:parkingFont lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentCenter];
    
    [self renderParkingFullness:[UIColor colorWithRed: 0.612 green: 0.847 blue: 0.42 alpha: 1] fullness:ParkingSpaceFullnessHalf];
    
}

//TODO - Make the space icon and circle dynamic
- (void) renderParkingFullness:(UIColor *)fillColor fullness:(ParkingSpaceFullness)fullness{
    //// Color Declarations
//    UIColor* fillColor = [UIColor colorWithRed: 0.612 green: 0.847 blue: 0.42 alpha: 1];
    UIColor* color3 = [UIColor colorWithRed: 0.571 green: 0.714 blue: 1 alpha: 1];
    
    //// Group
    {
        //// Bezier Drawing
        UIBezierPath* bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint: CGPointMake(91.5, 44.5)];
        [bezierPath addLineToPoint: CGPointMake(118.5, 44.5)];
        [fillColor setFill];
        [bezierPath fill];
        [fillColor setStroke];
        bezierPath.lineWidth = 1;
        [bezierPath stroke];
        
        
        //// Bezier 2 Drawing
        UIBezierPath* bezier2Path = [UIBezierPath bezierPath];
        [bezier2Path moveToPoint: CGPointMake(105.5, 30.33)];
        [bezier2Path addLineToPoint: CGPointMake(105.5, 58.34)];
        [fillColor setFill];
        [bezier2Path fill];
        [fillColor setStroke];
        bezier2Path.lineWidth = 1;
        [bezier2Path stroke];
        
        
        //// Oval 2 Drawing
        UIBezierPath* oval2Path = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(91, 30, 28, 29)];
        [color3 setStroke];
        oval2Path.lineWidth = 1;
        [oval2Path stroke];
        
        
        //// Oval Drawing
        CGRect ovalRect = CGRectMake(91, 30, 28, 29);
        UIBezierPath* ovalPath = [UIBezierPath bezierPath];
        [ovalPath addArcWithCenter: CGPointMake(0, 0) radius: CGRectGetWidth(ovalRect) / 2 startAngle: 270 * M_PI/180 endAngle: 90 * M_PI/180 clockwise: YES];
        [ovalPath addLineToPoint: CGPointMake(0, 0)];
        [ovalPath closePath];
        
        CGAffineTransform ovalTransform = CGAffineTransformMakeTranslation(CGRectGetMidX(ovalRect), CGRectGetMidY(ovalRect));
        ovalTransform = CGAffineTransformScale(ovalTransform, 1, CGRectGetHeight(ovalRect) / CGRectGetWidth(ovalRect));
        [ovalPath applyTransform: ovalTransform];
        
        [fillColor setFill];
        [ovalPath fill];
    }
}

- (void) setParkingLocation:(RPParkingLocation *)parkingLocation {
    _parkingLocation = parkingLocation;
    
    [self setNeedsDisplay];
}

@end
