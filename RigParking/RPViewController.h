//
//  RPViewController.h
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPBaseViewController.h"

@interface RPViewController : UIViewController <UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (strong, nonatomic) IBOutlet UISearchBar *mainSearchBar;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) RPBaseViewController * viewController;

- (id) initWithViewController:(RPBaseViewController *) viewController;

@end
