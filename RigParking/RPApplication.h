//
//  RPApplication.h
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPLocationManager.h"
#import "RPRestClient.h"
#import "RPBackendServer.h"
#import "RPUser.h"

@interface RPApplication : UIApplication

//@property (nonatomic, readonly) RPBackendServer * backendServer;

@property (nonatomic, readonly) RPLocationManager * locationManager;
@property (nonatomic, strong) NSArray * parkingLocations;
@property (nonatomic, assign) BOOL loggedIn;
@property (nonatomic, strong) RPUser * loggedInUser;
@property (nonatomic, readonly) RPRestClient *restClient;
 
@end
