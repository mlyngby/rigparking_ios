//
//  RPViewController.m
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPViewController.h"
#import "RPMapViewController.h"
#import "RPListViewController.h"
#import "RPCheckinViewController.h"
#import "RPLoginRegisterViewController.h"

@interface RPViewController ()
- (void) showChildViewController;
- (void) hideChildViewController;
@end

@implementation RPViewController {
    RPLoginRegisterViewController * _loginRegViewController;
}

- (id) initWithViewController:(RPBaseViewController *) viewController {
    self = [super initWithNibName:@"RPViewController" bundle:nil];
    if (self) {
        self.viewController = viewController;
        _loginRegViewController = [[RPLoginRegisterViewController alloc] initWithNibName:nil bundle:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [self configureNavBar:@"Rig Parking"];
    [self configureAppearance];
//    [self loadChildViewControllers];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self showChildViewController];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self hideChildViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
- (void) configureNavBar:(NSString *)title {
    
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:title];
    
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStyleBordered target:self action:@selector(settingsButtonTapped:)];
    self.navigationItem.leftBarButtonItem = settingsButton;
    
    UIImage *checkmarkImage = [UIImage imageNamed:@"checkmark.png"];
    UIBarButtonItem *checkmarkButton = [[UIBarButtonItem alloc] initWithImage:checkmarkImage style:UIBarButtonItemStylePlain target:self action:@selector(addButtonTapped:)];
    
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonTapped:)];

    navItem.leftBarButtonItem = settingsButton;
    navItem.rightBarButtonItem = checkmarkButton;
    
    self.navBar.items = @[navItem];
}

- (void) configureAppearance {
    [self.contentView setBackgroundColor:UIColorFromRGB(kBaseColor)];
    [self.view setBackgroundColor:UIColorFromRGB(kBaseColor)];
}

- (void) showChildViewController {
    if (!_viewController) {
        return;
    }
    [self addChildViewController:_viewController];
    _viewController.view.frame = self.contentView.bounds;
    [self.contentView addSubview:_viewController.view];
    [_viewController didMoveToParentViewController:self];
}

- (void) hideChildViewController {
    if (!_viewController) {
        return;
    }
    [_viewController willMoveToParentViewController:nil];
    [_viewController.view removeFromSuperview];
    [_viewController removeFromParentViewController];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    if (self.mainSearchBar.isFirstResponder) {
        [self.mainSearchBar findAndResignFirstResponder];
    }
}

#pragma mark - actions
- (void) settingsButtonTapped:(id)sender {
    DLog(@"Settings tapped");

}

- (void) addButtonTapped:(id)sender {
    DLog(@"Add tapped");
    if (!APP.loggedIn) {
        [self presentViewController:_loginRegViewController animated:YES completion:nil];
    }
    else {
        [UIAlertView quickTitle:@"Coming soon" message:@"will be able to check in to locations soon"];
    }
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    DLog(@"Changed.");
    
    if (searchText.length == 0) {
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
    }
}

@end
