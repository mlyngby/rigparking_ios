//
//  RPAppDelegate.h
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPViewController.h"

@interface RPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) RPViewController *viewController;

@end
