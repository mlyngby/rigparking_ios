//
//  RPApplication.m
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPApplication.h"

@implementation RPApplication

- (id) init {
    self = [super init];
    if (self) {
        _locationManager = [[RPLocationManager alloc] init];
        _loggedIn = NO;
        _restClient = [RPRestClient sharedInstance];
    }
    return self;
}

@end
