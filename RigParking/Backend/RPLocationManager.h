//
//  RPLocationManager.h
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define kSecondsToWaitForLocation 15

@interface RPLocationManager : NSObject <CLLocationManagerDelegate>

- (BOOL) beginLocatingUser;

@end
