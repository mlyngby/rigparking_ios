//
//  RPBackendServer.h
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RPBackendServer;

@protocol RPBackendServerDelegate <NSObject>

- (void) backendServer:(RPBackendServer *)server didFetchNearbyLocationsForCoordinate:(CLLocationCoordinate2D)coordinate withDistanceInKilimoters:(CGFloat)kilometers parkingLocations:(NSArray *)parkingLocations;

- (void) backendServer:(RPBackendServer *)server didFetchLocationsWithinCoordinates:(CLLocationCoordinate2D)nwCorner seCorner:(CLLocationCoordinate2D)seCorner parkingLocations:(NSArray *)parkingLocations;

@end

@interface RPBackendServer : NSObject

@property (weak, nonatomic) id<RPBackendServerDelegate> delegate;

+ (id) sharedInstance;

- (void) searchNearbyLocations:(CLLocationCoordinate2D)coordinate distanceInKilometers:(CGFloat)kilometers;
- (void) searchLocationsWithNwCorner:(CLLocationCoordinate2D)nwCorner seCorner:(CLLocationCoordinate2D)seCorner;

@end
