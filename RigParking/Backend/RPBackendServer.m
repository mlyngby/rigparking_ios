//
//  RPBackendServer.m
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPBackendServer.h"
#import "RPRestClient.h"
#import "RPParkingLocation.h"

@implementation RPBackendServer {
    RPRestClient * _restClient;
}

+ (id) sharedInstance {
    static RPBackendServer * __sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[RPBackendServer alloc] init];
    });
    
    return __sharedInstance;
}

- (id) init {
    self = [super init];
    if (self) {
        _restClient = [RPRestClient sharedInstance];
    }
    return self;
}

- (void) searchNearbyLocations:(CLLocationCoordinate2D)coordinate distanceInKilometers:(CGFloat)kilometers {
    __block NSArray *parkingLocations;
    __block NSMutableArray *parkingLocationModels;
    
    DLog(@"%@/parking/geo/%f/%f/%f/", _restClient.baseURL, kilometers, coordinate.latitude, coordinate.longitude);
    
    APP.parkingLocations = nil;
    
    [_restClient getPath:[NSString stringWithFormat:@"/parking/geo/%f/%f/%f/", kilometers, coordinate.latitude, coordinate.longitude] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        parkingLocations = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        parkingLocationModels = [NSMutableArray arrayWithCapacity:parkingLocations.count];
        [parkingLocations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *parkingDict = (NSDictionary *)obj;
            RPParkingLocation *parkingLoc = [RPParkingLocation parkingLocationFromDictionary:parkingDict];
            [parkingLocationModels addObject:parkingLoc];
        }];
        
        DLog(@"%@", parkingLocationModels);
        if (_delegate) {
            [_delegate backendServer:self didFetchNearbyLocationsForCoordinate:coordinate withDistanceInKilimoters:kilometers parkingLocations:parkingLocationModels];
            APP.parkingLocations = parkingLocationModels;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Geo Fail!");
    }];
    

}

- (void) searchLocationsWithNwCorner:(CLLocationCoordinate2D)nwCorner seCorner:(CLLocationCoordinate2D)seCorner {
    __block NSArray *parkingLocations;
    __block NSMutableArray *parkingLocationModels;
    
    NSString *path = [NSString stringWithFormat:@"/parkingBracket/geo/%f/%f/%f/%f/", nwCorner.latitude, nwCorner.longitude, seCorner.latitude, seCorner.longitude];
    
    DLog(@"%@", path);
    
    APP.parkingLocations = nil;
    
    [_restClient getPath:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        parkingLocations = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        parkingLocations = (NSArray *)responseObject;
        
        parkingLocationModels = [NSMutableArray arrayWithCapacity:parkingLocations.count];
        [parkingLocations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *parkingDict = (NSDictionary *)obj;
            RPParkingLocation *parkingLoc = [RPParkingLocation parkingLocationFromDictionary:parkingDict];
            [parkingLocationModels addObject:parkingLoc];
        }];
        
        DLog(@"%@", parkingLocationModels);
        if (_delegate) {
            [_delegate backendServer:self didFetchLocationsWithinCoordinates:nwCorner seCorner:seCorner parkingLocations:parkingLocationModels];
            APP.parkingLocations = parkingLocationModels;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Geo Fail!");
    }];
    
    
}

@end
