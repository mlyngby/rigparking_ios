//
//  RPRestClient.h
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "AFHTTPClient.h"

typedef void (^JSONResponseBlock)(NSDictionary *json);

@interface RPRestClient : AFHTTPClient

+ (id) sharedInstance;

@property (nonatomic, strong) NSDictionary *user;

- (BOOL) isAuthorized;

@end
