//
//  RPLocationManager.m
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPLocationManager.h"

@implementation RPLocationManager {
    CLLocationManager       * _locationManager;
    NSDate                  * _kickoffTimestamp;
    
    CLLocationCoordinate2D  _lastCoordinate;
    BOOL                    _updatedOnce;
}

- (id) init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (BOOL) beginLocatingUser {
    if (!_locationManager) {
        [self configureLocationManager];
    }
    
    _kickoffTimestamp = [NSDate date];
    
    BOOL enabled = [CLLocationManager locationServicesEnabled];
    
    if (!enabled) return enabled;
    
    [_locationManager startUpdatingLocation];
    
    return YES;
}

#pragma mark - Private Methods
- (void)configureLocationManager {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
}

#pragma mark - CLLocationManagerDelegate
- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    
    if (!_updatedOnce) {
        _updatedOnce = YES;
        _lastCoordinate = location.coordinate;
    }
    else {
        if (_lastCoordinate.latitude == location.coordinate.latitude && _lastCoordinate.longitude == location.coordinate.longitude) {
            [_locationManager stopUpdatingLocation];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserLocationPinpointedNotification object:nil userInfo:@{kUserLocationDictionaryKey : location}];
            
            DLog(@"Stopped updating and posted notification");
        }
        else {
            _lastCoordinate = location.coordinate;
        }
    }

    NSTimeInterval howRecent = [location.timestamp timeIntervalSinceNow];
    if (abs(howRecent) < kSecondsToWaitForLocation) {
        DLog(@"%+.6f lat, %+.6f lon", location.coordinate.latitude, location.coordinate.longitude);
    }
    
    if (abs([_kickoffTimestamp timeIntervalSinceNow]) > kSecondsToWaitForLocation) {
        [_locationManager stopUpdatingLocation];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserLocationPinpointedNotification object:nil userInfo:@{kUserLocationDictionaryKey : location}];
        
        DLog(@"Stopped updating and posted notification");
    }
    
}

@end
