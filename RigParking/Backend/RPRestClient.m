//
//  RPRestClient.m
//  RigParking
//
//  Created by Mason Lyngby on 2/3/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPRestClient.h"
#import "AFNetworking.h"

@implementation RPRestClient

+ (id) sharedInstance {
    static RPRestClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:kRestAPIBaseURL];
        __sharedInstance = [[RPRestClient alloc] initWithBaseURL:baseUrl];
    });
    
    return __sharedInstance;
}

- (id) initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    
    if (self) {
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        [self setDefaultHeader:@"Accept" value:@"application/json"];
        
        self.parameterEncoding = AFJSONParameterEncoding;
    }
    
    return self;
}

- (BOOL) isAuthorized {
    return [[_user valueForKey:@"id"] intValue] > 0;
}



@end
