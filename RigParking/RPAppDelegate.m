//
//  RPAppDelegate.m
//  RigParking
//
//  Created by Mason Lyngby on 2/1/13.
//  Copyright (c) 2013 Shipping Optimization, LLC. All rights reserved.
//

#import "RPAppDelegate.h"

#import "RPViewController.h"
#import "RPMapViewController.h"
#import "RPListViewController.h"

@interface RPAppDelegate ()
- (void) configureAppearance;
@end

@implementation RPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
//    self.viewController = [[RPViewController alloc] initWithNibName:@"RPViewController" bundle:nil];
    
    RPMapViewController *mapViewController = [[RPMapViewController alloc] initWithNibName:nil bundle:nil];
    RPListViewController *listViewController = [[RPListViewController alloc] initWithNibName:nil bundle:nil];
    
    RPViewController *mapContainerViewController = [[RPViewController alloc] initWithViewController:mapViewController];
    mapContainerViewController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFeatured tag:0];
    
    RPViewController *listContainerViewController = [[RPViewController alloc] initWithViewController:listViewController];
    listContainerViewController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:1];
    
    RPViewController *placeHolderViewController = [[RPViewController alloc] initWithNibName:nil bundle:nil];
    placeHolderViewController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:2];
    
    UITabBarController *mainTabBarController = [[UITabBarController alloc] init];
    mainTabBarController.viewControllers = @[mapContainerViewController, listContainerViewController, placeHolderViewController];
    
    self.window.rootViewController = mainTabBarController;
    [self.window makeKeyAndVisible];
    
    [self configureAppearance];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Private Methods
- (void) configureAppearance {
    [[UINavigationBar appearance] setTintColor:UIColorFromRGB(kBaseColor)];
    [[UIToolbar appearance] setTintColor:UIColorFromRGB(kBaseColor)];
    [[UISegmentedControl appearance] setTintColor:UIColorFromRGB(kBaseColor)];
    [[UISearchBar appearance] setTintColor:UIColorFromRGB(kBaseColor)];
}

@end
